<?php

/**
 * @author ArtyomVeselkov
 * @copyright 2016
 */

if ( ( !defined( 'BASEPATH' ) ) )
    die( 'Access dined' );
    
require_once( BASEPATH . 'utils.php' );
 
interface JArrayInterface {
    public static function set( $key, $object );
    
    public static function get( $key );
    
    public static function del( $key );
    
    public static function is( $key );
}
 
trait JArrayTrait {
    private static
        //! Associative array of stored objects
        $_data = array();
        
    /**
     * Add object to storage
     * 
     * @param $key string 
     * @param $object object
     * @return mixed
     */
    public static function set( $key, $object ) {
        try {
            if ( isset( static::$_data[ $key ] ) && ( $object !== $_data[ $key ] ) ) {
                static::del( $key );
            }
        } catch ( Exception $e ) {
            JLog::add( JUtils::C_ERROR, 'Enable to delete object `' . 
                get_class( $object ) . '\' under key: ' . $key 
            );
            return null;
        }
        return static::$_data[ $key ] = $object;
    }
    
    public static function get( $key ) {
        return isset( static::$_data[ $key ] ) ? static::$_data[ $key ] : null;
    }
    
    public static function del( $key ) {
        static::$_data[ $key ] = null;
        unset( static::$_data[ $key ] );
    }
    
    public static function is( $key ) {
        return isset( static::$_data[ $key ] );
    }
}

final class JStorage implements JArrayInterface {
    use JArrayTrait;
    
    final function __clone() {}
    
    final function __construct() {}
}

final class JConf implements JArrayInterface {
    const
        DEF_CONF_FILE = 'conf.php';
    
    use JArrayTrait;    
    
    public static function load( $file = '' ) {
        $file = ( ( '' != $file ) && ( file_exists( $file ) ) ) ? $file : BASEPATH . self::DEF_CONF_FILE;
        if ( file_exists( $file ) )
            static::$_data = require( $file );
    }
    
    final function __clone() {}
    
    final function __construct() {}  
}

class JLog implements JArrayInterface {
    use JArrayTrait;
    
    private static
        $_count = 0;
    
    public static function add( $code = JUtils::C_INFO, $text = '', $e = null ) {     
        $s = JUtils::fmtErrorMsg( $code, $text );
        $s .= ( null != $e ) ? "\n\texception code: " . $e->getCode() .  "\n\texception text:" . $e->getMessage() : '';
        self::set( self::$_count++, $s );
    } 
    
    public static function out() {
        var_dump( self::$_data );
    }
    
    public static function shd( $code = JUtils::C_INFO, $text = '', $e = null ) {
        self::add( $code, $text, $e );
        self::out();
        die( 'Termination via logger' );
    }
}

final class JLang implements JArrayInterface {
    const
        DEF_LANG_CODE = 'en';
    
    use JArrayTrait;    
    
    public function load( $code = '' ) {
        $file = BASEPATH . 'lang' . $code . '.php';
        $file = ( ( '' != $code ) && ( file_exists( $file ) ) ) ? 
            $file : 
            BASEPATH . 'lang' . self::DEF_CONF_FILE . '.php';
        if ( file_exists( $file ) )
            static::$_data = require( $file );
    }
    
    final function __clone() {}
    
    final function __construct() {}  
}

abstract class JObjectProto implements ArrayAccess, Countable {
    const
    //@{ Choose mode for collection params into array
        DEF_COLLAR_NONE   = 0,
        DEF_COLLAR_STATIC = 1,
        DEF_COLLAR_LOCAL  = 2;
    //@}
        
    private static
        //! Array for collection inner params cross all descendents
        $_static_collar   = array();        
        
    private
        //! Array of inner data
        $_data            = array(),        
        //! Array for collection inner params for current instance
        $_local_collar    = array(),
        //! Collection mode
        $_collar_mode     = self::DEF_COLLAR_NONE;
        
        
    /**
     * Clear collar's
     * If method `endCollar' was called previously -- both collars would be NULLed
     * 
     * @return void
     */
     
    private function flushCollar() {
        if ( self::DEF_COLLAR_LOCAL != $this->_collar_mode )
            $this->_static_collar = null;
        if ( self::DEF_COLLAR_STATIC != $this->_collar_mode )
            $this->_local_collar = null;
    }
    
    private function modeCollar( $mode = self::DEF_COLLAR_STATIC ) {
        $this->_collar_mode = $mode;
    }
        
    public static function instance() {
        $name = get_called_class();
        return JStorage::is( $name ) ?
            JStorage::get( $name ) :                    
            JStorage::set( 
                $name, 
                ( 0 < func_num_args()) ?
                    ( new ReflactionClass( $name ) )->NewInstanceArgs( func_get_args() ) :
                    new $name
            );  
    }
      
    public function offsetExists( $offset ) {
        return isset( $this->_data[ $offset ] );
    }
    
    public function offsetUnset( $offset ) {
        unset( $this->_data[ $offset ] );
    }
    
    public function offsetGet( $offset ) {
        if ( self::DEF_COLLAR_NONE == $this->_collar_mode )
            return $this->_data[ $offset ];
        if ( self::DEF_COLLAR_STATIC == $this->_collar_mode )
            $_static_collar[ $offset ] = $this->_data[ $offset ];
        else
            $_local_collar[ $offset ] = $this->_data[ $offset ];
        return $this;   
    }
    
    public function offsetSet( $offset, $value ) {
        return $this->_data[ $offset ] = $value;
    }
    
    public function count() {
        return count( $this->_data );
    }
    
    public function startStaticCollar() {
        modeCollar( self::DEF_COLLAR_STATIC );
    }
    
    public function startLocalCollar() {
        modeCollar( self::DEF_COLLAR_LOCAL );
    }
    
    public function endCollar() {
        if ( self::DEF_COLLAR_STATIC == $this->_collar_mode )
            return $this->_static_collar;
        if ( self::DEF_COLLAR_LOCAL == $this->_collar_mode )
            return $this->_local_collar;
        modeCollar( self::DEF_COLLAR_NONE );
    }
}


trait JModelProtoTrait {     
    private static 
        //! PDO instance
        $pdo      = null,
        $dbe      = '',
        $host     = '',
        $port     = '',
        $dbname   = '',
        $user     = '',
        $pwd      = '';
        
    private function init() {
        $dbname        = JConf::get( 'db_name' );
        $user          = JConf::get( 'db_user' );
        $pwd           = JConf::get( 'db_pwd' );
        $dbe           = JConf::get( 'db_dbe' );
        $host          = JConf::get( 'db_host' );
        $port          = JConf::get( 'db_port' );
      
        self::$dbname = $dbname;
        self::$user   = $user;
        self::$pwd    = '' != $pwd  ? $pwd  : '';        
        self::$dbe    = '' != $dbe  ? $dbe  : 'mysql';
        self::$host   = '' != $host ? $host : 'localhost';
        self::$port   = '' != $port ? $port : '3306';
        // self::charset = '' != $charset ? $charset : 'utf-8';
    }
}

abstract class JModelProto extends JObjectProto {
    use JModelProtoTrait;
        
    protected static
        //! Name of the table in DB, which current model belongs to
        $tblname  = '',
        $tf_gen   = array(),
        $tf_crit  = array(),
        $tf       = array();
        
    protected
        //! Query string
        $q        = '',
        //! Last fetched results
        $r        = null;
    
    function __construct() {
        if ( self::$pdo )
            return $this;
        
        self::init();    
        
        try {
            $dsn = self::$dbe . ':host=' . self::$host . '; port=' . self::$port . 
                '; dbname=' . self::$dbname . '; charset=utf8';
            self::$pdo = new PDO( $dsn, self::$user, self::$pwd );
            self::$pdo->setAttribute( PDO::ATTR_EMULATE_PREPARES, false );
            self::$pdo->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
        } catch ( PDOException $e ) {
            JLog::shd( JUtils::C_ERROR, 'Unable perform connection to the DB' .
                "\n\t Specified settings: " . $dsn, $e
                );
        }
        
        static::$tblname = JConf::get( 'db_tables_' . get_class() );
        
        static::$tf = ( null != JConf::get( 'preset_model_' . get_class() ) ) ? 
            array_merge( static::$tf_gen, (array)JConf::get( 'preset_model_' . get_class() ) ) : 
            static::$tf_gen; 
        
        staticself::searchCritical();                    
    }
    
    
    
    protected function processTblFields() {
        $f = current( static::$tf );
        $sql = '( ' . JUtils::fmtTblField( key( static::$tf ),$f );
        next( static::$tf );
        while ( $f = current( static::$tf ) ) {
            $sql .= ", \n" . JUtils::fmtTblField( key( static::$tf ), $f );
            next( static::$tf );
        }
        $sql .= ')';
        reset( static::$tf );

        return $sql;
    }
    
    protected function clearResult() {
        if ( $this->r ) {
            $this->r = null;
            unset( $this->r );        
        }
    }
    
    protected function push( $data = null ) {
        if ( !self::$pdo )
            return null;
            
        try {
            self::$pdo->beginTransaction();
            
            $sth = self::$pdo->prepare( $this->getQuery() );
            
            $sth->execute( $data );
            
            $this->clearResult();
            $this->r = $sth->fetchAll( PDO::FETCH_LAZY );
            
            self::$pdo->commit();
        } catch ( PDOException $e ) {
            JLog::add( JUtils::C_ERROR, 'Unable ti insert data into table' .
                "\n\t Query: " . $this->getQuery(), $e
            );
        }
        $this->addQuery( '', true );
    }
    
    protected static function filterData( $data, $data_preset = null ) {
        $data_f = array_intersect_key( $data, ( !$data_preset ) ? static::$tf : $data_preset );                   
        return $data_f;
    }
    
    protected static function searchCriticalData() {
        static::$tf_crit = array();
        foreach ( static::$tf as $key => $row )
            if ( (is_array( $row ) ) && isset( $row[ JConf::MODEL_TABLE_CRIT_ACC ] ) 
                && ( $row[ JConf::MODEL_TABLE_CRIT_ACC ] ) )
                static::$tf_crit = $key; 
    }
    
    public function addQuery( $q, $reset = false ) {
        $this->q = ( $reset ? $q : $this->q . $q );
        if ( '' != trim( $this->q ) )
            $this->q .= ";\n";
        return $this;
    }
    
    public function getQuery() {
        return $this->q;
    }    
    
    public function recreate() {
        $this->addQuery( 'DROP TABLE IF EXISTS `' . static::$tblname . "`" )->push();
        
        $this->addQuery( 'CREATE TABLE IF NOT EXISTS `' . static::$tblname . '` ' .
            $this->processTblFields() )->push();
    }
    
    public function insert( $data ) {
        $data_f = self::filterData( $data );
            
        $s_keys = '( `' . implode( '`, `', array_keys( $data_f ) ) . '` )';
        $s_phvs = '( :' . implode( ', :', array_keys( $data_f ) ) . ' )';
        
        if ( $data_f )
            $this->addQuery( 'INSERT INTO `' . static::$tblname . '` ' . $s_keys . ' VALUES ' . $s_phvs )
                ->push( $data_f );
        else
            return null;
        
    }
    
    public function load( $data ) {
        if ( !$data )
            return null;
        
        $data_f = filterData( $data, static::$tf_crit );
        if ( count( $data_f ) < count( static::$tf_crit ) )
            return null;
        
        $s_keys = '`' . implode( '`, `', array_keys( $data_f ) ) . '`';
        $s_stmt = implode( ', ', array_map( 
            function( $k, $v ){ 
                return '`' . static::$tblname . '`.`' . $k . '` = ' . 
                    ( is_numeric( $v ) ? (string)$v : '`' . (string)$v . '`' ); }, 
                array_keys( $data_f ), $data_f ) );        
        $this->addQuery( 'SELECT' . $s_keys .  ' FROM `' . static::$tblname . '` ' . "\nWHERE " . $s_stmt )
            ->push( $data_f );
            
    }
}

JConf::load();

?>