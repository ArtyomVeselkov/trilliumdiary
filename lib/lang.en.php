<?php

/**
 * @author AryomVeselkov
 * @copyright 2016
 */

return [
    'Trillium' => '«Trillium»',
    'Registration form',
    
    // HTML template :: index
    'Welcome to the Trillium family diary!',
    'View Profile Page',
    'Welcome, ',
    'Unauthorized user',
    
    'user_id_lbl' => 'Your ID',
    'user_nick_lbl' => 'Your nick',
    'user_pwd_lbl' =>  'Your password',
    'user_fn_lbl' => 'Your first name',
    'user_mn_lbl' => 'Your middle name',
    'user_ln_lbl' => 'Your last name',
    'user_role_lbl' => 'Who are you?',
    "In order to proceed to the diary you ought to create your family's account or choose already exists one"
    
];

?>