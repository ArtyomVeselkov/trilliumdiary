<?php

/**
 * @author ArtyomVeselkov
 * @copyright 2016
 */
 
if ( !defined( 'BASEPATH' ) )
    die( 'Access dined' );
    

define( 'BASEUTILS', true );

function _trim( &$var ) {
    $var = trim( $var );
}
 
function _fmtIfNotEmpty( $arr, $key, $ls = '', $rs = '', $es = '', $addsl = false, $bool = true ) {
    return isset(  $arr[ $key ] ) ? 
        $ls . ( $bool ? ( $addsl ? addslashes( $arr[ $key ] ) : $arr[ $key ] ) : '' ) . $rs : 
        $es;
}

function _isArrayNested( $arr ) {
    return ( ( is_array( $arr ) ) && ( is_array( array_values( $arr )[0] ) ) );
}

class JUtils { 
    const
        C_ERROR = -1,
        C_OK    =  0,
        C_INFO  =  1,
    
        MODEL_TABLE_TYPE_KEY   = 0,
        MODEL_TABLE_TYPE_INT   = 'INT',
        MODEL_TABLE_TYPE_VCH   = 'VARCHAR',
        MODEL_TABLE_TYPE_CHR   = 'CHAR',
        MODEL_TABLE_TYPE_ENM   = 'ENUM',
        MODEL_TABLE_TYPE_SET   = 'SET',
        MODEL_TABLE_TYPE_BLB   = 'BLOB',
        MODEL_TABLE_TYPE_TXT   = 'TXT',
        
        // Position in array: Size or ENUM( 'VAL1', 'VAL2' ) or SET( 'VAL1', 'VAL2' )
        MODEL_TABLE_SIZE_KEY   = 1,                
        
        //! Position in array: AUTO_INCREMENT
        MODEL_TABLE_KEYS_AI    = 2,
        //! Position in array: PRIMARY KEY
        MODEL_TABLE_KEYS_PK    = 3,
        //! Position in array: UNIQUE
        MODEL_TABLE_KEYS_UNI   = 4, 
        
        //! Position in array: default value
        MODEL_TABLE_DFLT_KEY   = 5,
        
        //! Not null
        MODEL_TABLE_NNUL_KEY   = 6,    
        
        MODEL_TABLE_COMM_KEY   = 7,
        
        MODEL_TABEL_FK_TABLE   = 8,
        
        MODEL_TABEL_FK_COL     = 9,
        
        MODEL_TABEL_FK_ONUPD   = 30,
        
        MODEL_TABLE_EXTR_KEY   = 10,
        
        //
        MODEL_TABLE_CRIT_ACC   = 11,
        
        MODEL_TABLE_ALVL_UPD   = 12,
        
        MODEL_TABLE_ULVL_UPD   = 13,
        
        MODEL_TABLE_ALVL_INS   = 14,
        
        MODEL_TABLE_ILVL_INS   = 15,
        
        MODEL_TABLE_LANG_LBL   = 20,
        
        MODEL_TABLE_LANG_DSC   = 21,        
        
        MODEL_TABEL_FK_ONDEL   = 31,
        
        MODEL_TABEL_FK_USEIT   = 32,
        
        // ! Goes as value for update table (value)
        MODEL_TABEL_REQ_VAL    = 33,
        
        // ! Goes as value for show (text)
        MODEL_TABEL_REQ_OUT    = 34,
        
        MODEL_TABEL_REQ_FMTSTR = 35,
        
        MODEL_TABEL_REQ_FMTVAL = 36,
        
        MODEL_TABLE_CONF_USEIT = 37,
        
        MODEL_TABLE_CONF_TABLE = 38,
        
        MODEL_TABLE_CONF_COLMN = 39,
        
        MODEL_TABLE_2DTYPE_KEY = 40,
        
        MODEL_TABLE_2DTYPE_PWD = 'pwd',
        
        
        
        
        VIEW_REQ_TYPE_REQ = 0,
        VIEW_REQ_TYPE_RES = 1;
        
        
        public static
            $model_view = array(
                JUtils::MODEL_TABLE_TYPE_KEY => array(
                    JUtils::MODEL_TABLE_TYPE_INT => 'number',
                    JUtils::MODEL_TABLE_TYPE_ENM => 'radio',
                    JUtils::MODEL_TABLE_TYPE_SET => 'checkbox',
                    JUtils::MODEL_TABLE_TYPE_VCH => 'text', 
                    JUtils::MODEL_TABLE_TYPE_CHR => 'text',
                    JUtils::MODEL_TABLE_TYPE_BLB => 'file',
                    JUtils::MODEL_TABLE_TYPE_TXT => 'text'
                ),
                JUtils::MODEL_TABLE_2DTYPE_PWD => array(
                    JUtils::MODEL_TABLE_2DTYPE_PWD => 'password'
                ),
                // size, set, enum
                JUtils::MODEL_TABLE_SIZE_KEY => 's',
                JUtils::MODEL_TABLE_ILVL_INS => 'show'
            );
            
        /**
         * Print value (is exists) from `$arr[ $key ]', append `$ls' at the beginning, `rs' -- at the end
         * Helper function to make code more compact
         * 
         * @param $es string place istead of `$arr[ $key ]', if it's empty
         * @param $addsl boolean if true -- escape some danger chars
         * @param $bool boolean if true -- print value of `$arr[ $key ]', else print nothing in this place
         * 
         */
        public static function fmtTblField( $col, $f ) {
            if ( '' != trim( $col ) )
                return ' `' . $col . '` ' .
                    _fmtIfNotEmpty( $f, self::MODEL_TABLE_TYPE_KEY, ' ', '', '' ) .
                    ( ( isset( $f[ self::MODEL_TABLE_SIZE_KEY ] ) ) ? ( ( is_numeric( $f[ self::MODEL_TABLE_SIZE_KEY ] ) ) ?
                        _fmtIfNotEmpty( $f, self::MODEL_TABLE_SIZE_KEY, '(', ') ', '' ) :
                        _fmtIfNotEmpty( $f, self::MODEL_TABLE_SIZE_KEY, '(', ') ', '' )
                    ) : '' ) .
                    _fmtIfNotEmpty( $f, self::MODEL_TABLE_KEYS_AI, ' AUTO_INCREMENT ', '', ' ', false, false ) .
                    _fmtIfNotEmpty( $f, self::MODEL_TABLE_KEYS_PK, ' PRIMARY KEY ', '', '', false, false ) . 
                    _fmtIfNotEmpty( $f, self::MODEL_TABLE_KEYS_UNI, ' UNIQUE ', '', '', false, false ) .
                    ( ( isset( $f[ self::MODEL_TABLE_DFLT_KEY ] ) ) ? ( ( is_numeric( $f[ self::MODEL_TABLE_DFLT_KEY ] ) ) ?    
                        _fmtIfNotEmpty( $f, self::MODEL_TABLE_DFLT_KEY, ' DEFAULT ', ' ', '', false, true  ) :
                        _fmtIfNotEmpty( $f, self::MODEL_TABLE_DFLT_KEY, ' DEFAULT `', '` ', '', false, true  )
                    ) : '' ) .
                    _fmtIfNotEmpty( $f, self::MODEL_TABLE_NNUL_KEY, ' ', ' null ', '' ) . 
                    _fmtIfNotEmpty( $f, self::MODEL_TABLE_COMM_KEY, ' COMMENT \'', '\' ', ' ', true )
                    
                    // Unsupported by MySQL syntaxes
                    /*
                    . ( $f[ self::MODEL_TABEL_FK_USEIT ] ? 
                        _fmtIfNotEmpty( $f, self::MODEL_TABEL_FK_COL, ' FOREIGN KEY REFERENCES ' .
                            _fmtIfNotEmpty( $f, self::MODEL_TABEL_FK_TABLE, '', '(', '(' ),
                            ')' . _fmtIfNotEmpty( $f, self::MODEL_TABEL_FK_ONUPD, ' ON UPDATE ', ' ', '' )
                                . _fmtIfNotEmpty( $f, self::MODEL_TABEL_FK_ONDEL, ' ON DELETE ', ' ', '' ) 
                            , '' ) : 
                        ''
                    )*/
                ;
            else
                return _fmtIfNotEmpty( $f, self::MODEL_TABLE_EXTR_KEY, ' ', ' ', '' ) ;
        } 
        
        public static function fmtErrorMsg( $code, $text ) {
            switch ( $code ) {
                case self::C_ERROR:
                {
                    return '[' . time() . '] Error: ' . $text;
                    break;
                }
                case self::C_OK:
                {
                    return '[' . time() . '] OK message: ' . $text;
                    break;
                }
                case self::C_INFO:
                {
                    return '[' . time() . '] Info: ' . $text;
                    break;
                }
            }
        }
        
        public static function filterArrBy1KeyNVal( &$arr, $filter ) {
            foreach ( $arr as $arr_key => $arr_row )
                foreach ( $filter as $f_key => $f_w ) {
                    if ( isset( $arr_row[ $f_key ] ) )
                        if ( is_array( $f_w ) )
                            if ( in_array( $arr_row[ $f_key ], $f_w) )
                                ;
                            else
                                unset( $arr[ $arr_key ] );
                        elseif ( $arr_row[ $f_key ] === $f_w )
                            ;
                        else
                            unset( $arr[ $arr_key ] );
                    else
                        unset( $arr[ $arr_key ] );                
                }
        } 
        
        /**
         * Rather strict mode
         */
        public static function validate2DArray( &$arr ) {
            array_filter( $arr, '_trim' );
            reset( $arr );
            while ( $item = current( $arr ) ) {
                // $item = filter_var()
                next( $arr );
            }
            reset( $arr );
        }
        
        /**
        *	Convert header string send by user into priority array
        * 
        *   
        *	@return void
        *	@param $value value of header string
        *   @example
        *   // will results [ 'ru-RU', '']
        *   print_r explodeAcceptHeaderByQ( 'ru-RU,ru;q=0.9,en;q=0.8' );
        **/
        public static function explodeAcceptHeaderByQ( $value, &$arr ) {
            $arr_in = explode( ',', $value );
            $arr = array();
            
            while ( $arr_in ) {
                $str   = trim( array_pop( $arr_in ) ); 
                $count = preg_match_all( '/([\w\/\*\-]+)(;level=([\d]+))?(;q=([\d\.]+))?/', $str, $matched, PREG_SET_ORDER );
                if ( $count > 0 ) {
                    $q = filter_var(
                        isset( $matched[0][5] ) ? $matched[0][5] : 1, 
                        FILTER_VALIDATE_FLOAT,
                        array(
                            'options' => array(
                                'default' => 1,
                            )
                        )
                    );
                    $arr[$matched[0][1]] = $q;
                }
            }
            arsort( $arr );
        }
    
}
?>