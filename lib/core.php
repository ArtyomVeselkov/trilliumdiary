<?php

/**
 * @author ArtyomVeselkov
 * @copyright 2016
 */
 
 
 /**
  * extension=php_pdo_mysql.dll is needed
  * php version > ...
  * 
  */

define ('BASEPATH', pathinfo(__FILE__, PATHINFO_DIRNAME ) . '/');

require_once(BASEPATH . 'base.php');

trait JModelTrait 
{
    private 
        //! ID is an array of values of primary key for each row in the corresponded table
        $_ids    = array();

    public static function getTableName() 
    {
        return $this->tblname;
    }
    

    public function loadByData($data) 
    {
       $this->selectAll($data);
       $r = $this->getResult();
       if (!$r)
           return null;
       
       foreach ($r as $key => $value)
           $this[$key] = $value;
       $this->_ids = array_merge(array_keys($r), $this->_ids);
    }
    
    public function loadById($id) 
    {
       $this->selectAll(array($this->tf_PK => $id));
       $r = $this->getResult();
       if (!$r)
           return null;
       
       foreach ($r as $key => $value)
           $this[$key] = $value;
       $this->_ids = array_merge(array_keys($r), $this->_ids);
    }
    
    /**
     * 
     * @param array if null -- will be used critical fields
     */
    public function getFilteredValues($id, $param = null)
    {
        if (!in_array($id, $this->getIds()))
            return null;

        return $this->filterData($this[$id], $param ? $param : $this->tf_crit);
    }
    
    public function readConjValuesById($param, $data)
    {
       $r = null;
       if ($this->selectConjValues(array($this->tf_PK => $id)))       
           $r = $this->getResult();
       if (!$r)
           return null;
       
       $out = array();
       foreach ($r as $key => $value)
           $out[$key] = $value;
       return $out;
    }
    
    public function save($addFilterA = null, $addFilterU = null) 
    {
        if (0 == $this->getCount())
            return null;
        foreach ($this->_ids as $id)
            $this->update($this[$id], $id, $addFilterA, $addFilterU);
    }
    
    public function remove($ids = null) 
    {
        if (0 == $this->getCount())
            return null;
            
        $ids = null == $ids ? $this->_ids[0] : $ids;        
        $ids = is_array($ids) ? 
            array_intersect_key($this->_ids, $ids) :
            in_array($ids, $this->_ids) ? array($ids) : array() 
        ;
        
        foreach ($ids as $id)
            $this->delete($id);  
    }
    
    public function build($data, $addFilterA = null, $addFilterI = null) 
    {
        $data = _isArrayNested($data) ? $data : array($data);
        foreach ($data as $data_item) {             
            if ($this->insert($data_item, $addFilterA, $addFilterI)) 
                $this->loadById($this->getLastInsertID());
        }
    }
    
    public function setSafe($key, $val, $id = null) 
    {
        if (0 == $this->getCount())
            return null;
            
        $id = null == $id ? $this->_ids[0] : $id;
        if (in_array($id, $this->_ids) && (isset($this[$id]))) {
            $p = $this[$id];
            $p[$key] = $val;
            $this[$id] = $p;
        }
    }
    
    public function getSafe($key, $id = null) 
    {
        if (0 == $this->getCount())
            return null;
            
        $id = null == $id ? $this->_ids[0] : $id;
        if ((in_array($id, $this->getIds())) && (isset($this[$id][$key])))
             return $this[$id][$key];
        return null;
    }
    
    public function getCount() 
    {
        return count($this->_ids);
    } 
    
    public function getIds() {
        return $this->_ids;
    }
    /**
     * 
     * 
     * @param boolean If true -- return only fields that are necessary for new insertion, false -- all table's fields
     */
    
    public function pack($select = true) 
    {
        return $select ? $this->tf_crit : $this->tf;
    }
    
    public function getPKID() 
    {
        if (0 == $this->getCount())
            return null;
            
        return array_map(
            function ($k) { return array($this->$tf_PK => $k); },
            $this->_ids
        ); 
    }
}

class JModelRoles extends JModelProto 
{
    use JModelTrait;
}

class JModelUser extends JModelProto 
{
    use JModelTrait;
}

class JModelFamily extends JModelProto 
{
    use JModelTrait;
}

class JModelRelations extends JModelProto 
{
    use JModelTrait;
}


class JModelDiary extends JModelProto 
{
    use JModelTrait;
}

class JModelFamilyStatus extends JModelProto
{
    use JModelTrait;
} 

class JControl extends JObjectProto 
{
    
    protected static function convertData2View($data_in, &$data_out, $grants, $grant) {
        $data_out = array();
        
        foreach ($data_in as $key => $row) {
            $_data = null;
            $_data = array();
            
            $_data['name']  = $key;
            $_data['value'] = '';
            
            foreach ($row as $field => $param) {
                switch ($field) {
                    case JUtils::MODEL_TABLE_TYPE_KEY:
                        $_data['data_type'] = JUtils::$model_view[JUtils::MODEL_TABLE_TYPE_KEY][$param];
                        break;
                    case JUtils::MODEL_TABLE_LANG_LBL:
                        $_data['label_str'] = JView::instance()->translate($param);
                        break;
                    case JUtils::MODEL_TABLE_LANG_DSC:
                    {
                        $_data['description_str'] = JView::instance()->translate($param);
                        break;
                    }
                    case $grant: 
                        $_data['show'] = in_array($param, $grants);
                        break;
                }
            }
            $data_out[$key] = $_data;
        }
    }    
}

class JUser extends JControl 
{    
    const DEF_GRANTS_VAL       = 'X';
    const DEF_USER_ROLE_FIELD  = 'fs_role';
    const DEF_USER_GRANT_FIELD = 'user_role';
        
    private
        $_user_grants        = null,
        $_user_id            = 0; 
        
        
    private function getGrantsDefault()
    {
        return array(self::DEF_GRANTS_VAL);
    }
        
    private function loadGrants()
    {
        $ju = JModelUser::instance();
        if (!$this->isLogged())
            $this->_user_grants = $this->getGrantsDefault();
        else {
             $roles = JModelUser::instance()->readConjValuesById(self::DEF_USER_ROLE_FIELD, $this->getUserId());
             $this->_user_grants = JModelFamilyStatus::instance()->readConjValuesById(self::DEF_USER_GRANT_FIELD, $roles[0]);
        }
            
    }
    
    public function getGrants()
    {
        if (!$this->_user_grants)
            $this->loadGrants();
        return $this->_user_grants;
    }
    
    private function setUserId($id)
    {
        if (in_array($id, JModelUser::instance()->getIds()))
            $this->_user_id = $id;
    }
    
    public function getUserId()
    {
        return $this->_user_id;
    }
    
    private function selectActiveUser()
    {
        $this->setUserId(JModelUser::instance()->getIds()[0]);
        $data = JModelUser::instance()->getFilteredValues($this->getUserId());
        $this->loadGrants();
        JCore::setCookies($data);
    }
        
    public function isLogged() 
    {
        return (JModelUser::instance()->getCount() > 0);
    }
    
    private function onNotLogged()
    {
        $ju = JModelUser::instance();
        /*if (JConf::get('start_page') != $jc::$uri_base)
            $jc::redirect(JConf::get('start_page'), 401, true)
            ;
        else {*/
            self::convertData2View( 
                $ju->pack(false), 
                $data_out, 
                $this->getGrants(),
                JUtils::MODEL_TABLE_ILVL_INS
            );
            JView::instance()->registerBox( 
                'registration-form', 
                $data_out, 
                JUtils::VIEW_REQ_TYPE_REQ, 
                'register',
                JConf::get('start_page') 
            );
            
            self::convertData2View( 
                $ju->pack(true), 
                $data_out,
                $this->getGrants(),
                JUtils::MODEL_TABLE_ILVL_INS
            );
            JView::instance()->registerBox( 
                'login-form', 
                $data_out, 
                JUtils::VIEW_REQ_TYPE_REQ, 
                'login',
                JConf::get('start_page') 
            );
        /*}*/
    }
    
    private function loginByCookies()
    {
        if ($this->isLogged())
            return true;
            
        $jc = JCore::instance();
        $ju = JModelUser::instance();
        
        $ju->loadByData($jc::$cookies);
    }
    
    public function register() 
    {
        if ($this->isLogged())
            return true;
            
        $jc = JCore::instance();
        
        JModelUser::instance()->build($jc::$post);
        if (!$this->isLogged()) {
            $this->onNotLogged();
            return false;
        } else
            $this->selectActiveUser();
        return true;
    }
    
    public function login() 
    {        
        if ($this->isLogged())
            return true;
            
        $jc = JCore::instance();
        $ju = JModelUser::instance();
        
        $ju->loadByData($jc::$post);
        if (!$this->isLogged()) {
            $this->onNotLogged();
            return false;
        } else
            $this->selectActiveUser();
        return true;
    }
    
    public function getUserData($key) 
    {
        return JModelUser::instance()->getSafe($key);
    }
    
    function __construct() 
    {
        $jc = JCore::instance();
        $this->loginByCookies();
        $jc::registerAction(get_class($this), 'register', 'register');  
        $jc::registerAction(get_class($this), 'login', 'login');
        
        if (!$this->isLogged() && (!in_array($jc::$post[JCore::ACTION_PARAM_NAME], array('login', 'register')))) {
            $this->onNotLogged();
            return false;
        }
        return true;        
    }
}

class JView extends JObjectProto 
{
    const
        DEF_BOX_PREFIX = 'box_';
    
    function __construct() {
        $jc = JCore::instance();
        $langs_pref_str = $jc::getServerValue('Accept-Language');
        if ($langs_pref_str)
            JUtils::explodeAcceptHeaderByQ($langs_pref_str, $langs_pref_arr);        
        $this['lang'] = $this->loadLang($langs_pref_arr);
    }    
    
    private function loadLang($langs_prior) {
        while ($lang = current($langs_prior)) {
            $file = BASEPATH . 'lang.' . key($langs_prior) . '.php';
            if (file_exists($file)) {
                JLang::load(key($langs_prior));
                return key($langs_prior);
            } else {
                next($langs_prior);
            }
        }
        JLang::load();
        return JLang::DEF_LANG_CODE;
    }
    
    public function translate($str) {
        $str_t = JLang::get($str);
        return (null != $str_t) ? $str_t : $str;
    }
    
    public function registerBox($box_name, $data, $mode, $event, $uri) {
        $html = "\n" . '<form action="' . $uri . '" method="POST">' . "\n" . '<div class="form-group">';
        if (JUtils::VIEW_REQ_TYPE_REQ == $mode)
            foreach ($data as $k => $r) {
                if ($r[ 'show' ])
                    $html .= "\n\t" . '<label for="id-' . $r['name'] . '">' . $r['label_str'] . '</label>' 
                             . "\n\t\t" . '<input class="form-control" id="id-' . $r['name'] . '" name="' . $r['name'] . '" ' 
                             . 'type="' . $r['data_type'] . '" '
                             . 'value="' . $r['value'] . '" '
                             . '/>'  
                             . "\n\t";
            }
            $html .= "\n\t" . '<input type="hidden" name="' . JCore::ACTION_PARAM_NAME . '" value="' . $event . '" />';
            $html .= '<button type="submit" class="btn btn-default">Submit</button>';
            $html .= "\n</div>\n" . '</form>';                        
            
            $this[self::DEF_BOX_PREFIX . $box_name] = $html;
    }
}

// JCore оперирует только другими контроллерами, которые оперируют уже данными
final class JCore extends JObjectProto 
{
    const ACTION_PARAM_NAME        = 'JCORE_ACTION';
    const GET_PREFIX_PROLONGATE    = 'pr-'; 
    //! 60*60*24*14
    const DEF_COOKIES_PROLONGATION = 31104000;
        
    private static
        $onAction = array();
        
    
    public static
        $server      = array(),
        $server_http = array(),
        $cookies     = array(),
        $post        = array(),
        $get         = array(),
        $get_prol    = array(),       
        $uri_base    = '',
        $uri_cur     = '',
        $method      = '';
        
    public 
        $view     = null;
            
    private function init() 
    {
        foreach ($_SERVER as $k => $v)
            if (false !== strpos($k, 'HTTP'))
                self::$server_http[strtolower(str_replace('_', '-', substr($k, strpos($k, '_') + 1)))] = $v;
            else
                self::$server[strtolower($k)] = $v;
                      
        foreach ($_COOKIE as $k => $v)  
            //self::$cookies[$k] = urldecode($v);
            self::$cookies[$k] = $v;
            
        self::$post     = $_POST;
        self::$get      = $_GET;
        foreach (self::$get as $k => $v) {
            $sub_key = substr($k, 0, strlen(self::GET_PREFIX_PROLONGATE));
            if (self::GET_PREFIX_PROLONGATE == $sub_key)
                self::$get_prol[$sub_key] = $v;
        }
        
        self::$uri_cur  = self::$server[ 'SCRIPT_NAME' ];
        
        self::$uri_base = self::$server[ 'SCRIPT_NAME' ];
    }
    
    public static function getServerValue($key)
    {
        $param = strtolower($key);
        if (isset(self::$server[$param]))
            return self::$server[$param];
        elseif (isset(self::$server_http[$param]))
            return self::$server_http[$param];
        return null; 
    }
        
    public static function registerAction($class, $action, $func) 
    {
        self::$onAction[$action] = array( $class => $func );
    }
    
    public static function redirect($url, $code = 303, $force = true) 
    {
		if (headers_sent()) {
            if (true == $force)  
                exit('<script type="text/javascript">window.location.href="' . $url . '";</script>');
            else
                echo '<script type="text/javascript">window.location.href="' . $url . '";</script>';
		} else {
		    header('Location: ' . $url, true, $statusCode);
		    if (true == $force)
                exit();
		}    
    }

    public static function setCookies($data)
    {
        foreach ($data as $k => $v)
            setcookie($k, urlencode($v), time() + self::DEF_COOKIES_PROLONGATION );
    }
    
    function __construct() 
    {
        self::init();
    }
    
    public function run() {
        $this->view = JView::instance();
        
        if (func_num_args() > 0)
            foreach (func_get_args() as $component)
                try {
                    $component::instance();
                } catch (exception $e) {
                    JLog::add(JUtils::C_ERROR, 'Can\'t create instance named: ' . $component, $e);
                }
                
        if (isset(self::$post[self::ACTION_PARAM_NAME]))
            foreach (self::$onAction[self::$post[ self::ACTION_PARAM_NAME]] as $class => $method)
                try {
                    if (method_exists($class::instance(), $method))
                        return call_user_func(array($class::instance(), $method));
                } catch (exception $e){
                    JLog::add(JUtils::C_ERROR, 'Can\'t call method `' . $method . '\' from that class: ' . $class, $e);
                } 
    }
    
    function __call($name, $args) {
        $method = substr($name, strpos($name, '_' ) + 1);
        switch (substr($name, 0, strpos($name, '_'))) {
            case 'view': 
                if (method_exists($this->view, $method))
                    return call_user_func_array(array($this->view, $method), $args);
                break;
            case 'conf':
                if ('get' == $method)
                    return (JConf::get($args[0]));
                break;                
            case 'user':
                if (method_exists(JUser::instance(), $method))
                    return call_user_func_array(array(JUser::instance(), $method), $args);
                break;
        }
    }  
}

return JCore::instance();

?>