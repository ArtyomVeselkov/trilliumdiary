<?php

/**
 * @author ArtyomVeselkov
 * @copyright 2016
 */
 
/**
 * TODO replace is_numeric with another function, which will checks both -- current type 
 *      and specified type in the preset  
 */

if ((!defined( 'BASEPATH')))
    die('Access dined');
    
require_once( BASEPATH . 'utils.php' );
 
interface JArrayInterface 
{
    public static function set($key, $object);
    
    public static function get($key);
    
    public static function del($key);
    
    public static function is($key);
}
 
trait JArrayTrait 
{
    private static
        //! Associative array of stored objects
        $_data = array();
        
    /**
     * Add object to storage
     * 
     * @param $key string 
     * @param $object object
     * @return mixed
     */
    public static function set($key, $object) 
    {
        try {
            if (isset(static::$_data[$key]) && ($object !== $_data[$key])) {
                static::del($key);
            }
        } catch (exception $e) {
            JLog::add( JUtils::C_ERROR, 'Enable to delete object `' . 
                get_class($object) . '\' under key: ' . $key 
            );
            return null;
        }
        return static::$_data[$key] = $object;
    }
    
    public static function get($key) 
    {
        return isset(static::$_data[$key]) ? static::$_data[$key] : null;
    }
    
    public static function del($key) 
    {
        static::$_data[$key] = null;
        unset(static::$_data[$key] );
    }
    
    public static function is($key) 
    {
        return isset(static::$_data[$key]);
    }
}

final class JStorage implements JArrayInterface 
{
    use JArrayTrait;
    
    final function __clone() {}
    
    final function __construct() {}
}

final class JConf implements JArrayInterface 
{
    const
        DEF_CONF_FILE = 'conf.php';
    
    use JArrayTrait;    
    
    public static function load($file = '') {
        $file = (('' != $file) && (file_exists($file))) ? $file : BASEPATH . self::DEF_CONF_FILE;
        if (file_exists($file))
            static::$_data = require( $file );
    }
    
    final function __clone() {}
    
    final function __construct() {}  
}

class JLog implements JArrayInterface 
{
    use JArrayTrait;
    
    private static
        $_count = 0;
    
    public static function add($code = JUtils::C_INFO, $text = '', $e = null) {     
        $s = JUtils::fmtErrorMsg($code, $text);
        $s .= (null != $e) ? "\n\texception code: " . $e->getCode() .  "\n\texception text:" . $e->getMessage() : '';
        self::set(self::$_count++, $s);
    } 
    
    public static function out() 
    {
        var_dump(self::$_data);
    }
    
    public static function shd($code = JUtils::C_INFO, $text = '', $e = null) 
    {
        self::add($code, $text, $e);
        self::out();
        die('Termination via logger');
    }
    
    function __toString()
    {
        self::out();
    }
}

final class JLang implements JArrayInterface 
{
    const
        DEF_LANG_CODE = 'en';
    
    use JArrayTrait;    
    
    public static function load($code = '') {
        $file = BASEPATH . 'lang' . $code . '.php';
        $file = (('' != $code) && (file_exists($file))) ? 
            $file : 
            BASEPATH . 'lang.' . self::DEF_LANG_CODE . '.php';
        if (file_exists($file))
            static::$_data = require($file);
    }

    
    final function __clone() {}
    
    final function __construct() {}  
}

abstract class JObjectProto implements ArrayAccess, Countable 
{
    const
    //@{ Choose mode for collection params into array
        DEF_COLLAR_NONE   = 0,
        DEF_COLLAR_STATIC = 1,
        DEF_COLLAR_LOCAL  = 2;
    //@}
        
    private static
        //! Array for collection inner params cross all descendents
        $_static_collar   = array();        
        
    private
        //! Array of inner data
        $_data            = array(),        
        //! Array for collection inner params for current instance
        $_local_collar    = array(),
        //! Collection mode
        $_collar_mode     = self::DEF_COLLAR_NONE;
        
        
    /**
     * Clear collar's
     * If method `endCollar' was called previously -- both collars would be NULLed
     * 
     * @return void
     */
     
    private function flushCollar() {
        if (self::DEF_COLLAR_LOCAL != $this->_collar_mode)
            $this->_static_collar = null;
        if (self::DEF_COLLAR_STATIC != $this->_collar_mode)
            $this->_local_collar = null;
    }
    
    private function modeCollar($mode = self::DEF_COLLAR_STATIC) 
    {
        $this->_collar_mode = $mode;
    }
    
    protected function getData() 
    {
        return $this->_data;
    }
        
    public static function instance() 
    {
        $name = get_called_class();
        return JStorage::is($name) ?
            JStorage::get($name) :                    
            JStorage::set( 
                $name, 
                (0 < func_num_args()) ?
                    (new ReflectionClass($name))->NewInstanceArgs(func_get_args()) :
                    new $name
            );  
    }
      
    public function offsetExists( $offset ) 
    {
        return isset($this->_data[$offset]);
    }
    
    public function offsetUnset($offset) {
        unset($this->_data[$offset]);
    }
    
    public function offsetGet($offset) 
    {
        if (self::DEF_COLLAR_NONE == $this->_collar_mode)
            return $this->_data[$offset];
        if (self::DEF_COLLAR_STATIC == $this->_collar_mode)
            $_static_collar[$offset] = $this->_data[$offset];
        else
            $_local_collar[$offset] = $this->_data[$offset];
        return $this;   
    }
    
    public function offsetSet($offset, $value)
    {
        return $this->_data[$offset] = $value;
    }
    
    public function count() 
    {
        return count($this->_data);
    }
    
    public function startStaticCollar() 
    {
        modeCollar(self::DEF_COLLAR_STATIC);
    }
    
    public function startLocalCollar() 
    {
        modeCollar(self::DEF_COLLAR_LOCAL);
    }
    
    public function endCollar() 
    {
        if (self::DEF_COLLAR_STATIC == $this->_collar_mode)
            return $this->_static_collar;
        if (self::DEF_COLLAR_LOCAL == $this->_collar_mode)
            return $this->_local_collar;
        modeCollar(self::DEF_COLLAR_NONE);
    }
}


trait JModelProtoTrait 
{     
    private static 
        //! PDO instance
        $pdo      = null,
        $dbe      = '',
        $host     = '',
        $port     = '',
        $dbname   = '',
        $user     = '',
        $pwd      = '';
        
    private function init() {
        $dbname        = JConf::get('db_name');
        $user          = JConf::get('db_user');
        $pwd           = JConf::get('db_pwd');
        $dbe           = JConf::get('db_dbe');
        $host          = JConf::get('db_host');
        $port          = JConf::get('db_port');
      
        self::$dbname = $dbname;
        self::$user   = $user;
        self::$pwd    = '' != $pwd  ? $pwd  : '';        
        self::$dbe    = '' != $dbe  ? $dbe  : 'mysql';
        self::$host   = '' != $host ? $host : 'localhost';
        self::$port   = '' != $port ? $port : '3306';
        // self::charset = '' != $charset ? $charset : 'utf-8';
    }
}

abstract class JModelProto extends JObjectProto 
{
    use JModelProtoTrait;
    
    private
        $lid      = null;
        
    protected
        //! Name of the table in DB, which current model belongs to
        $tblname  = '',
        $tf_PK    = '',
        $tf_extra = array(),
        $tf_crit  = array(),
        $tf       = array();
        
    protected
        //! Query string
        $q        = '',
        //! Last fetched results
        $r        = null;
    
    function __construct() 
    {
        if (!self::$pdo) {
        
            self::init();    
            
            try {
                $dsn = self::$dbe . ':host=' . self::$host . '; port=' . self::$port . 
                    '; dbname=' . self::$dbname . '; charset=utf8';
                    
                self::$pdo = new PDO( $dsn, self::$user, self::$pwd );
                self::$pdo->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
                self::$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            } catch (PDOException $e) {
                JLog::shd( JUtils::C_ERROR, 'Unable perform connection to the DB' .
                    "\n\t Specified settings: " . $dsn, $e
                );
            }
        }
        
        $this->tblname  = JConf::get('db_tables_' . get_called_class());
        
        $this->tf       = JConf::get('preset_model_' . get_called_class() .'_cols');
        $this->tf_extra = JConf::get('preset_model_' . get_called_class() .'_extra');
        
  
        $this->tf_crit  = static::filterFields(array( JUtils::MODEL_TABLE_CRIT_ACC => true));
        $this->tf_PK    = array_keys(static::filterFields(array( JUtils::MODEL_TABLE_KEYS_PK => true)))[0];
    }
        
    private function setLastInsertID($reset = false) 
    {
        $this->lid = ($reset ? null : self::$pdo->lastInsertId());           
    }
    
    public function getLastInsertID() 
    {
        return $this->lid;           
    }
    
    protected function processTblFields() 
    {  
        if (is_array($this->tf_extra))
            $fields = array_merge($this->tf, $this->tf_extra);
        else
            $fields = $this->tf;
        // Need in order to be sure, that pointer is not somewhere else rather than at the beginning
        reset($fields);
        $f = current($fields);
        $sql = '( ' . JUtils::fmtTblField(key( $fields ),$f);
        next( $fields );
        while ( $f = current($fields)) {
            $sql .= ", \n" . JUtils::fmtTblField(key($fields), $f);
            next($fields);
        }
        $sql .= ')';
        
        $fields = null;
        unset($fields);

        return $sql;
    }
    
    protected function clearResult() 
    {
        if ($this->r) {
            $this->r = null;
            unset($this->r);        
        }
    }
    
    protected static function implodePK() 
    {
        return '`' . $this->tf_PK . '` = :' . $this->tf_PK;
    } 


    
    protected function push($data = null) {
        if (!self::$pdo)
            JLog::shd(JUtils::C_ERROR, 'There is no active connection to the DB');
            
        try {
            self::$pdo->beginTransaction();
            
            $sth = self::$pdo->prepare($this->q);
            
            $sth->execute($data);
            $this->SetLastInsertID();
            
            self::$pdo->commit();
            
            $this->clearResult();
            $this->r = $sth->fetchAll(PDO::FETCH_ASSOC | PDO::FETCH_UNIQUE);            
        } catch (PDOException $e) {
            $this->addQuery('', true);
            self::$pdo->rollback();
            $this->setLastInsertID(true);
            JLog::add( JUtils::C_ERROR, 'Unable to insert data into table' .
                "\n\t Query: " . $this->getQuery(), $e
            );
            return false;
        }
        $this->addQuery('', true);
        return true;
    }
    protected function isEnoughData($data, $forSelect = false) 
    {
        if (($forSelect) && (isset($data[$this->tf_PK])))
            return true;
        else
            return (count($this->filterData($data, $this->tf_crit)) >= count($this->tf_crit));
    }
    
    protected function filterData($data, $data_preset = null, $add_data = null) 
    {
        $data_f = array_intersect_key( 
            $data, 
            (!$add_data ? array() : $add_data) + 
                ((!$data_preset) ? $this->tf : $data_preset) 
        );                   
        return $data_f;
    }
    
    protected function filterFields($param) 
    {
        if ((!$this->tf) || (!is_array($param)))
            return null;
        $tf_out = array();
        
        foreach ($this->tf as $tf_key => $tf_field) {
            $arr = array_intersect_key($tf_field, $param);
            if ($arr)
                 $tf_out[$tf_key] = $tf_field;
        }
        return $tf_out;
    }
    
    protected function filterDataComplex(&$data_out, $data_in, $addFilterAName = null, 
        $addFilterA = null, $addFilterXName = null,  $addFilterX  = null) 
    {
       
        $data_out = $this->filterData( 
            $data_in, 
            null,
            (((count($addFilterA) > 0) && ($addFilterAName)) ? 
                array_map( 
                    function($v)
                    { 
                        return array(JUtils::$addFilterAName => $v); 
                    }, 
                    $addFilterA
                ) :
                null
            )
        );
        
        if ($addFilterX && $addFilterXName) 
            JUtils::filterArrBy1KeyNVal( 
                $data_f,
                array_map( 
                    function($v)
                    { 
                        return array($addFilterXName => $v); 
                    }, 
                    $addFilterX
                )
            );
    }
    
    public function addQuery($q, $reset = false) 
    {
        $this->q = $reset ? $q : $this->q . $q;
        if ('' != trim($this->q))
            $this->q .= ";\n";
        return $this;
    }
    
    public function getQuery() 
    {
        return $this->q;
    }    
    
    public function getResult() 
    {
        return $this->r;
    }
    
    public function recreate($prepare = true) 
    {      
        if ($prepare) {            
            $stmt = 'SELECT CONSTRAINT_NAME FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS'
                . "\n\t" .'WHERE CONSTRAINT_SCHEMA = DATABASE()' 
                . "\n\t\t" . 'AND TABLE_NAME = \'' . $this->tblname . '\''
                . "\n\t\t" . 'AND CONSTRAINT_TYPE = \'FOREIGN KEY\'';
            
            if ($this->addQuery($stmt)->push())    
                foreach($this->getResult() as $FK_key => $FK_val)
                    $this->addQuery( 'ALTER TABLE `' . $this->tblname . '` DROP FOREIGN KEY `' . $FK_key . '`')
                        ->push();
        } else {                
            $this->addQuery('DROP TABLE IF EXISTS `' . $this->tblname . "`")->push();
            
            if ($this->addQuery('CREATE TABLE IF NOT EXISTS `' . $this->tblname . '` ' .
                $this->processTblFields())->push()) {                
                $FKs = $this->tf;
                JUtils::filterArrBy1KeyNVal($FKs, array(JUtils::MODEL_TABEL_FK_USEIT => array(true)));
                
                foreach ($FKs as $key => $val) {
                    $this->addQuery( 'ALTER TABLE `' . $this->tblname . '`'
                        . "\n\t" . 'ADD CONSTRAINT fk_' . $this->tblname . $key . '_' 
                        . JConf::get('db_tables_' . $val[JUtils::MODEL_TABEL_FK_TABLE]) . $val[JUtils::MODEL_TABEL_FK_COL] . ' '
                        . "\n\t" . 'FOREIGN KEY (' . $key . ') '
                        . "\n\t" . 'REFERENCES ' . JConf::get('db_tables_' . $val[JUtils::MODEL_TABEL_FK_TABLE]) . '(' . $val[JUtils::MODEL_TABEL_FK_COL] . ')'
                        . "\n\t" . ' ON UPDATE ' . $val[JUtils::MODEL_TABEL_FK_ONUPD] . ' ON DELETE ' .  $val[JUtils::MODEL_TABEL_FK_ONDEL]
                    )->push();
                }
            }
        } 
    }
    
    public function insert($data, $addFilterA = null, $addFilterI = null ) 
    {
        if ((!$data) || (!$this->isEnoughData($data)))
            return null;

        $this->filterDataComplex( 
            $data_f, $data, 
            $addFilterA, JUtils::MODEL_TABLE_ALVL_INS,
            $addFilterI, JUtils::MODEL_TABLE_ILVL_INS
        );
            
        $s_keys = '( `' . implode( '`, `', array_keys( $data_f ) ) . '` )';
        $s_phvs = '( :' . implode( ', :', array_keys( $data_f ) ) . ' )';
        
        if ($data_f)
            return $this->addQuery('INSERT INTO `' . $this->tblname . '` ' . $s_keys . ' VALUES ' . $s_phvs)
                ->push($data_f);
        else
            return false;
    }
    
    public function selectAll($data) 
    {
        if ((!$data) || (!$this->isEnoughData($data,true)))
            return null;
        
        $this->filterDataComplex($data_f, $data);

        $s_keys = '`' . implode('`, `', array_keys($this->tf)) . '`';
        
        $s_stmt = implode(' AND ', array_map( 
            function($k, $v){ 
                return '`' . $this->tblname . '`.`' . $k . '` = ' . 
                      ':' . $k; },
                array_keys($data_f), $data_f) );
                
        $this->addQuery('SELECT ' . $s_keys .  ' FROM `' . $this->tblname . '` ' 
            . $s_join . "\n\tWHERE " . $s_stmt)->push($data_f);
    }
    /**
     * Extended variant of wrapper function for SQL's SELECT 
     * 
     * @return boolean true if OK, else -- false 
     */
    public function selectConjValues($param, $data)
    {
        if ((!array_key_exists($param, $this->tf)) && (!$data))
            return false;
            
        $select = array($this->tf[JUtils::MODEL_TABEL_FK_TABLE] => $this->tf[JUtils::MODEL_TABEL_FK_COL]);
        $select[] += $this->tf[JUtils::MODEL_TABEL_REQ_VAL];
        
        $this->filterDataComplex( 
            $data_f, $data, 
            $addFilterA, JUtils::MODEL_TABLE_ULVL_UPD,
            $addFilterU, JUtils::MODEL_TABLE_ULVL_UPD
        );

        $s_select = implode(', ', array_map( 
            function($t, $k)
            { 
                return '`' . $t . '`.`' . $k . '`'; 
            },
            array_keys($select), $select) 
        );
        
        $s_from = $this->tf[JUtils::MODEL_TABEL_FK_TABLE];
        
        $s_join = implode(', ', array_merge(array_values($data_f)));
        
        $s_on = '`' . $this->tblname . '`.`' . $param . '` = `' . $this->tf[JUtils::MODEL_TABEL_FK_TABLE] . '`.' 
            . $this->tf[JUtils::MODEL_TABEL_FK_COL] . '`';
        
        $s_where = implode(' AND ', array_map( 
            function($k, $v){ 
                return '`' . $this->tblname . '`.`' . $k . '` = ' . 
                      ':' . $k; },
                array_keys($data_f), $data_f) 
        );
                
        $this->addQuery('SELECT ' . $s_keys .  ' FROM `' . $this->tblname . '` ' 
            . $s_join . "\n\tWHERE " . $s_stmt)->push($data_f);  
    }
    
    public function update($data, $ids, $addFilterA = null, $addFilterU = null) 
    {
        if ((!$data) || (!$ids))
            return null;

        $this->filterDataComplex( 
            $data_f, $data, 
            $addFilterA, JUtils::MODEL_TABLE_ULVL_UPD,
            $addFilterU, JUtils::MODEL_TABLE_ULVL_UPD
        );
        
        $s_stmt = implode( ', ', array_map( 
            function( $k )
            { 
                return '`' . $this->tblname . '`.`' . $k . '` = :' . $k; 
            },
                    array_keys($data_f)) 
        );
        
        if ($data_f)
            $this->addQuery( 'UPDATE `' . $this->tblname . '` SET ' . $s_stmt . ' WHERE ' . self::implodePK())
                ->push(array_merge( $data_f, array_combine( $this->tf_PK, $ids)));
        else
            return null;
    }
    
    public function delete($ids) 
    {
        if ((!$this->tf_PK) || (!$ids))
            return null;  
            
        $this->addQuery('DELETE FROM `' . $this->tblname . '` ' . ' WHERE ' . self::implodePK())
                ->push(array_combine($this->tf_PK, $ids));
    }
    
    /*
    public function extrenalQuery( $cols, $from, $join ) {
        $s_from =  implode( ', ', array_map( 
            function( $k ){ 
                return '`' . $k . '`, ' . $k; },
                array_keys( $from ) ) 
        );
        
        foreach ( $from as $tbl_name => $keys ) {
            $s_where .= implode( ', `'  . $tbl_name . '`.', array_map( 
                function( $k ){ 
                    return '`'. $k . '` = :' . $k; },
                        array_keys( $keys ) ) 
            ); 
            
            
            
            $s_where =.   
    }
    */
}

JConf::load();

?>