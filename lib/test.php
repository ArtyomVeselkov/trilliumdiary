<?php

/**
 * @author ArtyomVeselkov
 * @copyright 2016
 */


$jr = JModelRoles::instance();
$jr->recreate(true);

$jfs = JModelFamilyStatus::instance();
$jfs->recreate(true);

$ju = JModelUser::instance();
$ju->recreate(true);


$jr->recreate(false);
$jfs->recreate(false);
$ju->recreate(false);

$rp = array(
    array(
        'role' => 0,
        'grants' => 'X'
    ),
    array(
        'role' => 1,
        'grants' => 'A,B,C,E,F'
    ),
    array(
        'role' => 2,
        'grants' => 'A,B,D,E,F'
    ),
    array(
        'role' => 3,
        'grants' => 'A,B,E'
    ),
    array(
        'role' => 4,
        'grants' => 'A,B,C,D,E,F,G'
    ),
);
$jr->build($rp);

$fs = array(
    array(
        'fs_id'   => 0,
        'fs_role' => 2,
        'fs_name' => 'Mother'
    ),
    array(
        'fs_id'   => 1,
        'fs_role' => 1,
        'fs_name' => 'Father'
    ),
    array(
        'fs_id'   => 2,
        'fs_role' => 3,
        'fs_name' => 'Child'
    )
);
$jfs->build($fs);

$u1 = array(
    'user_nick' => 'JABach',
    'user_first_name' => 'Johann',
    'user_middle_name' => 'Ambrosius',
    'user_last_name' => 'Bach',
    'user_pwd_hash' => '3',
    'user_role' => '2',
    'user_fid' => '1'
    );
    
$uu = array( 
    array(
    'user_nick' => '1',
    'user_first_name' => 'Johann',
    'user_middle_name' => 'Ambrosius',
    'user_last_name' => 'Bach',
    'user_pwd_hash' => '3',
    'user_role' => '2',
    'user_fid' => '1'
    ),
    array(
    'user_nick' => '2',
    'user_first_name' => 'Johann',
    'user_middle_name' => 'Ambrosius',
    'user_last_name' => 'Bach',
    'user_pwd_hash' => '3',
    'user_role' => '2',
    'user_fid' => '1'
    ),
    array(
    'user_nick' => '3',
    'user_first_name' => 'Johann',
    'user_middle_name' => 'Ambrosius',
    'user_last_name' => 'Bach',
    'user_pwd_hash' => '3',
    'user_role' => '2',
    'user_fid' => '1'
    ),
);

$ju->build( array( 'user_nick' => 'JABach1', 'user_pwd_hash' => '3' ) );
$ju->build( $u1 );
$ju->build( $uu );

$ju['user_last_name'] = 'BACH123';
$ju->setSafe('user_nick', '1000', 5); 

$ju->save();
$ju->remove();


?>