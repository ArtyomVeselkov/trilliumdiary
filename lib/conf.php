<?php

/**
 * @author ArtyomVeselkov
 * @copyright 2016
 */
 
if ( !defined( 'BASEPATH' ) )
    die( 'Access dined' );
 
require_once( BASEPATH . 'utils.php' );

return [
    'db_tables_JModelUser' => 'JDiray_ModelUser',
    'db_tables_JModelRoles' => 'JDiray_ModelRoles',
    'db_tables_JModelFamilyStatus' => 'JDiray_ModelFamilyStatus',
    'db_tables_JModelRelations' => 'JDiray_JModelRelations',
    
    'db_dbe'       => 'mysql',
    'db_host'      => 'localhost',
    'db_name'      => 'jDiary',
    'db_user'      => 'root',    
    'db_pwd'       => '', 
    
    
    // TODO: !!! Make it load from XML file
    'preset_model_JModelUser_cols' => array(    
        'uid' => array (
            JUtils::MODEL_TABLE_TYPE_KEY => JUtils::MODEL_TABLE_TYPE_INT,
            JUtils::MODEL_TABLE_SIZE_KEY => 10,
            JUtils::MODEL_TABLE_KEYS_AI  => true,
            JUtils::MODEL_TABLE_KEYS_PK  => true,
            JUtils::MODEL_TABLE_COMM_KEY => 'user\'s unique ID',
            JUtils::MODEL_TABLE_ULVL_UPD => 'G',
            JUtils::MODEL_TABLE_ILVL_INS => 'Z',
            JUtils::MODEL_TABLE_LANG_LBL => 'user_id_lbl',
            JUtils::MODEL_TABLE_LANG_DSC => 'user_id_dsc'
            
        ),
        'user_nick' => array (
            JUtils::MODEL_TABLE_TYPE_KEY => JUtils::MODEL_TABLE_TYPE_VCH,
            JUtils::MODEL_TABLE_SIZE_KEY => 32,
            JUtils::MODEL_TABLE_KEYS_UNI => true,
            JUtils::MODEL_TABLE_NNUL_KEY => 'NOT',
            JUtils::MODEL_TABLE_COMM_KEY => 'nick for login',
            JUtils::MODEL_TABLE_ULVL_UPD => 'E',
            JUtils::MODEL_TABLE_CRIT_ACC => true,
            JUtils::MODEL_TABLE_ILVL_INS => 'X',
            JUtils::MODEL_TABLE_LANG_LBL => 'user_nick_lbl',
            JUtils::MODEL_TABLE_LANG_DSC => 'user_nick_dsc'
        ),
        'user_pwd_hash' => array (
            JUtils::MODEL_TABLE_TYPE_KEY => JUtils::MODEL_TABLE_TYPE_CHR,
            JUtils::MODEL_TABLE_SIZE_KEY => 64,
            JUtils::MODEL_TABLE_NNUL_KEY => 'NOT',
            JUtils::MODEL_TABLE_COMM_KEY => 'password\'s hash',
            JUtils::MODEL_TABLE_ULVL_UPD => 'E',
            JUtils::MODEL_TABLE_CRIT_ACC => true,
            JUtils::MODEL_TABLE_ILVL_INS => 'X',
            JUtils::MODEL_TABLE_LANG_LBL => 'user_pwd_lbl',
            JUtils::MODEL_TABLE_LANG_DSC => 'user_pwd_dsc'
        ),
        'user_first_name' => array (
            JUtils::MODEL_TABLE_TYPE_KEY => JUtils::MODEL_TABLE_TYPE_VCH,
            JUtils::MODEL_TABLE_SIZE_KEY => 32,
            JUtils::MODEL_TABLE_NNUL_KEY => 'NOT',
            JUtils::MODEL_TABLE_COMM_KEY => 'ex. Sam',
            JUtils::MODEL_TABLE_ULVL_UPD => 'E',
            JUtils::MODEL_TABLE_ILVL_INS => 'X',
            JUtils::MODEL_TABLE_LANG_LBL => 'user_fn_lbl',
            JUtils::MODEL_TABLE_LANG_DSC => 'user_fn_dsc'
        ),
        'user_middle_name' => array (
            JUtils::MODEL_TABLE_TYPE_KEY => JUtils::MODEL_TABLE_TYPE_VCH,
            JUtils::MODEL_TABLE_SIZE_KEY => 32,
            JUtils::MODEL_TABLE_NNUL_KEY => 'NOT',
            JUtils::MODEL_TABLE_ULVL_UPD => 'E',
            JUtils::MODEL_TABLE_ILVL_INS => 'X',
            JUtils::MODEL_TABLE_LANG_LBL => 'user_mn_lbl',
            JUtils::MODEL_TABLE_LANG_DSC => 'user_mn_dsc'
        ),
        'user_last_name' => array (
            JUtils::MODEL_TABLE_TYPE_KEY => JUtils::MODEL_TABLE_TYPE_VCH,
            JUtils::MODEL_TABLE_SIZE_KEY => 32,
            JUtils::MODEL_TABLE_NNUL_KEY => 'NOT',
            JUtils::MODEL_TABLE_ULVL_UPD => 'E',
            JUtils::MODEL_TABLE_ILVL_INS => 'X',
            JUtils::MODEL_TABLE_LANG_LBL => 'user_ln_lbl',
            JUtils::MODEL_TABLE_LANG_DSC => 'user_ln_dsc'
        )/*,
        'user_role' => array (
            JUtils::MODEL_TABLE_TYPE_KEY => JUtils::MODEL_TABLE_TYPE_INT,
            JUtils::MODEL_TABLE_SIZE_KEY => 4,
            JUtils::MODEL_TABLE_COMM_KEY => '0 - child, 1 - mother, 2 - father, 3 - admin',
            JUtils::MODEL_TABLE_ULVL_UPD => 'F',
            JUtils::MODEL_TABLE_ILVL_INS => 'X',
            JUtils::MODEL_TABEL_FK_TABLE => 'JModelFamilyStatus',
            JUtils::MODEL_TABEL_FK_COL   => 'fs_role',
            JUtils::MODEL_TABEL_FK_ONUPD => 'CASCADE',
            JUtils::MODEL_TABEL_FK_ONDEL => 'SET null',
            JUtils::MODEL_TABEL_FK_USEIT => true,
            JUtils::MODEL_TABEL_REQ_VAL  => 
                array(
                    array( 'JModelFamilyStatus' => 'fs_role' ),
                    array( 'JModelFamilyStatus' => 'fs_name' )
                ),
            // if not defined -- use concat
            JUtils::MODEL_TABEL_REQ_FMTSTR => '%s (%s)',
            JUtils::MODEL_TABEL_REQ_FMTVAL => 
                array(
                    // first -- order # for sptintf by MODEL_TABEL_REQ_FMTSTR string
                    // second -- use translation of value
                    array( 1, false, ), // firstly goes role name, ...
                    array( 0, true ) // then only it's id
                ),
            JUtils::MODEL_TABLE_LANG_LBL => 'user_role_lbl',
            JUtils::MODEL_TABLE_LANG_DSC => 'user_role_dsc'
        ),
        'user_fid' => array (
            JUtils::MODEL_TABLE_TYPE_KEY   => JUtils::MODEL_TABLE_TYPE_INT,
            JUtils::MODEL_TABLE_SIZE_KEY   => 10,
            JUtils::MODEL_TABLE_DFLT_KEY   => 0,
            JUtils::MODEL_TABLE_NNUL_KEY   => 'NOT',
            JUtils::MODEL_TABLE_COMM_KEY   => 'ID of family user belongs to',
            JUtils::MODEL_TABLE_ULVL_UPD   => 'E',
            JUtils::MODEL_TABLE_ILVL_INS   => 'A',
            JUtils::MODEL_TABLE_CONF_USEIT => true,
            JUtils::MODEL_TABLE_CONF_TABLE => ''
            JUtils::MODEL_TABLE_CONF_COLMN =>
            JUtils::MODEL_TABLE_LANG_LBL   => 'user_family_id_lbl',
            JUtils::MODEL_TABLE_LANG_DSC   => 'user_family_id_dsc'
        ),*/
    ),
    
    'preset_model_JModelUser_extra' => array(
        '' => array(
            JUtils::MODEL_TABLE_EXTR_KEY => 'KEY `nick` (`user_nick`)'
        ),
        ' ' => array(
            JUtils::MODEL_TABLE_EXTR_KEY => 'KEY `fuid` (`uid`, `user_fid`)'
        ) 
                
    ),
    
    'preset_model_JModelRoles_cols' => array(    
        'role' => array (
            JUtils::MODEL_TABLE_TYPE_KEY => JUtils::MODEL_TABLE_TYPE_INT,
            JUtils::MODEL_TABLE_SIZE_KEY => 4,
            JUtils::MODEL_TABLE_KEYS_PK  => true,
            JUtils::MODEL_TABLE_COMM_KEY => '',
            JUtils::MODEL_TABLE_ULVL_UPD => 'G',
            JUtils::MODEL_TABLE_ILVL_INS => 'G'
        ),
        'grants' => array (
            JUtils::MODEL_TABLE_TYPE_KEY => JUtils::MODEL_TABLE_TYPE_SET,
            JUtils::MODEL_TABLE_SIZE_KEY => "A, B, C, D, E, F, G, H, X, Z",
            JUtils::MODEL_TABLE_COMM_KEY => 'A - view, B - mark, C - distribute, D - upload, E - edit personal info, F - parent changes, G - admin, X - new user, Z -nobody',
            JUtils::MODEL_TABLE_CRIT_ACC => true,
            JUtils::MODEL_TABLE_ULVL_UPD => 'G',
            JUtils::MODEL_TABLE_ILVL_INS => 'G'
        ),
        'name' => array (
            JUtils::MODEL_TABLE_TYPE_KEY => JUtils::MODEL_TABLE_TYPE_CHR,
            JUtils::MODEL_TABLE_SIZE_KEY => 18,
            JUtils::MODEL_TABLE_NNUL_KEY => 'NOT',
            JUtils::MODEL_TABLE_ULVL_UPD => 'G',
            JUtils::MODEL_TABLE_ILVL_INS => 'G'
        )
    ),
    
    'preset_model_JModelFamilyStatus_cols' => array(    
        'fs_id' => array (
            JUtils::MODEL_TABLE_TYPE_KEY => JUtils::MODEL_TABLE_TYPE_INT,
            JUtils::MODEL_TABLE_SIZE_KEY => 4,
            JUtils::MODEL_TABLE_KEYS_PK  => true,
            JUtils::MODEL_TABLE_COMM_KEY => '',
            JUtils::MODEL_TABLE_ULVL_UPD => 'G',
            JUtils::MODEL_TABLE_ILVL_INS => 'G'
        ),
        'fs_role' => array (
            JUtils::MODEL_TABLE_TYPE_KEY => JUtils::MODEL_TABLE_TYPE_INT,
            JUtils::MODEL_TABLE_SIZE_KEY => 4,
            JUtils::MODEL_TABLE_DFLT_KEY => 0,
            JUtils::MODEL_TABLE_COMM_KEY => '',
            JUtils::MODEL_TABLE_ULVL_UPD => 'G',
            JUtils::MODEL_TABLE_ILVL_INS => 'G',
            JUtils::MODEL_TABLE_CRIT_ACC => true,
            JUtils::MODEL_TABEL_FK_TABLE => 'JModelRoles',
            JUtils::MODEL_TABEL_FK_COL   => 'role',
            JUtils::MODEL_TABEL_FK_ONUPD => 'CASCADE',
            JUtils::MODEL_TABEL_FK_ONDEL => 'SET null',
            JUtils::MODEL_TABEL_FK_USEIT => true,
            JUtils::MODEL_TABEL_REQ_VAL  => 
                array(
                    array( 'JModelRoles' => 'role' ),
                    array( 'JModelRoles' => 'name' )
                ),
            // if not defined -- use concat
            JUtils::MODEL_TABEL_REQ_FMTSTR => '%s (%s)',
            JUtils::MODEL_TABEL_REQ_FMTVAL => 
                array(
                    // first -- order # for sptintf by MODEL_TABEL_REQ_FMTSTR string
                    // second -- use translation of value
                    array( 1, false, ), // firstly goes role name, ...
                    array( 0, true ) // then only it's id
                )
        ),
        'fs_name' => array (
            JUtils::MODEL_TABLE_TYPE_KEY => JUtils::MODEL_TABLE_TYPE_CHR,
            JUtils::MODEL_TABLE_SIZE_KEY => 18,
            JUtils::MODEL_TABLE_NNUL_KEY => 'NOT',
            JUtils::MODEL_TABLE_ULVL_UPD => 'G',
            JUtils::MODEL_TABLE_ILVL_INS => 'G'
        )
    ),
    
    'preset_model_JModelFamily_cols' => array(    
        'f_id' => array (
            JUtils::MODEL_TABLE_TYPE_KEY => JUtils::MODEL_TABLE_TYPE_INT,
            JUtils::MODEL_TABLE_SIZE_KEY => 10,
            JUtils::MODEL_TABLE_KEYS_AI  => true,
            JUtils::MODEL_TABLE_KEYS_PK  => true,
            JUtils::MODEL_TABLE_COMM_KEY => '',
            JUtils::MODEL_TABLE_ULVL_UPD => 'G',
            JUtils::MODEL_TABLE_ILVL_INS => 'G'
        ),
        'f_pwd' => array (
            JUtils::MODEL_TABLE_TYPE_KEY   => JUtils::MODEL_TABLE_TYPE_INT,
            JUtils::MODEL_TABLE_SIZE_KEY   => 4,
            JUtils::MODEL_TABLE_DFLT_KEY   => 0,
            JUtils::MODEL_TABLE_COMM_KEY   => '',
            JUtils::MODEL_TABLE_2DTYPE_KEY => JUtils::MODEL_TABLE_2DTYPE_PWD,
            JUtils::MODEL_TABLE_ULVL_UPD   => 'H',
            JUtils::MODEL_TABLE_ILVL_INS   => 'H',
            JUtils::MODEL_TABLE_CRIT_ACC   => true
        ),        
        'f_name' => array (
            JUtils::MODEL_TABLE_TYPE_KEY => JUtils::MODEL_TABLE_TYPE_CHR,
            JUtils::MODEL_TABLE_SIZE_KEY => 32,
            JUtils::MODEL_TABLE_KEYS_UNI => true,
            JUtils::MODEL_TABLE_NNUL_KEY => 'NOT',
            JUtils::MODEL_TABLE_ULVL_UPD => 'H',
            JUtils::MODEL_TABLE_ILVL_INS => 'H'
        )
    ),  
    
    'preset_model_JModelRelations_cols' => array(    
        'rel_id' => array (
            JUtils::MODEL_TABLE_TYPE_KEY => JUtils::MODEL_TABLE_TYPE_INT,
            JUtils::MODEL_TABLE_SIZE_KEY => 10,
            JUtils::MODEL_TABLE_KEYS_PK  => true,
            JUtils::MODEL_TABLE_COMM_KEY => '',
            JUtils::MODEL_TABLE_ULVL_UPD => 'G',
            JUtils::MODEL_TABLE_ILVL_INS => 'G'
        ),
        'rel_uid' => array (
            JUtils::MODEL_TABLE_TYPE_KEY => JUtils::MODEL_TABLE_TYPE_INT,
            JUtils::MODEL_TABLE_SIZE_KEY => 4,
            JUtils::MODEL_TABLE_DFLT_KEY => 0,
            JUtils::MODEL_TABLE_COMM_KEY => '',
            JUtils::MODEL_TABLE_ULVL_UPD => 'G',
            JUtils::MODEL_TABLE_ILVL_INS => 'G',
            JUtils::MODEL_TABLE_CRIT_ACC => true,
            JUtils::MODEL_TABEL_FK_TABLE => 'JModelUser',
            JUtils::MODEL_TABEL_FK_COL   => 'uid',
            JUtils::MODEL_TABEL_FK_ONUPD => 'CASCADE',
            JUtils::MODEL_TABEL_FK_ONDEL => 'SET null',
            JUtils::MODEL_TABEL_FK_USEIT => true,
            JUtils::MODEL_TABEL_REQ_VAL  => 
                array(
                    array( 'JModelUser' => 'user_first_name' ),
                    array( 'JModelUser' => 'user_middle_name' ),
                    array( 'JModelUser' => 'user_last_name' )
                ),
            // if not defined -- use concat
            JUtils::MODEL_TABEL_REQ_FMTSTR => '%s %s %s',
            JUtils::MODEL_TABEL_REQ_FMTVAL => 
                array()
        ),        
        'rel_famstatus' => array (
            JUtils::MODEL_TABLE_TYPE_KEY => JUtils::MODEL_TABLE_TYPE_INT,
            JUtils::MODEL_TABLE_SIZE_KEY => 4,
            JUtils::MODEL_TABLE_COMM_KEY => '',
            JUtils::MODEL_TABLE_ULVL_UPD => 'A',
            JUtils::MODEL_TABLE_ILVL_INS => 'A',
            JUtils::MODEL_TABLE_CRIT_ACC => true,
            JUtils::MODEL_TABEL_FK_TABLE => 'JModelFamilyStatus',
            JUtils::MODEL_TABEL_FK_COL   => 'fs_id',
            JUtils::MODEL_TABEL_FK_ONUPD => 'CASCADE',
            JUtils::MODEL_TABEL_FK_ONDEL => 'SET null',
            JUtils::MODEL_TABEL_FK_USEIT => true,
            JUtils::MODEL_TABEL_REQ_VAL  => 
                array(
                    array( 'JModelFamilyStatus' => 'fs_id' ),
                    array( 'JModelFamilyStatus' => 'fs_name' )
                ),
            // if not defined -- use concat
            JUtils::MODEL_TABEL_REQ_FMTSTR => '%s (%s)',
            JUtils::MODEL_TABEL_REQ_FMTVAL => 
                array(
                    // first -- order # for sptintf by MODEL_TABEL_REQ_FMTSTR string
                    // second -- use translation of value
                    array( 1, false, ), // firstly goes role name, ...
                    array( 0, true ) // then only it's id
                )
        ),        
        'rel_fid' => array (
            JUtils::MODEL_TABLE_TYPE_KEY => JUtils::MODEL_TABLE_TYPE_INT,
            JUtils::MODEL_TABLE_SIZE_KEY => 10,
            JUtils::MODEL_TABLE_COMM_KEY => '',
            JUtils::MODEL_TABLE_ULVL_UPD => 'A',
            JUtils::MODEL_TABLE_ILVL_INS => 'A',
            JUtils::MODEL_TABLE_CRIT_ACC => true,
            JUtils::MODEL_TABLE_CONF_USEIT => true,
            JUtils::MODEL_TABLE_CONF_TABLE => 'JModelFamily',
            JUtils::MODEL_TABLE_CONF_COLMN => 'f_pwd',
            JUtils::MODEL_TABEL_FK_TABLE => 'JModelFamily',
            JUtils::MODEL_TABEL_FK_COL   => 'f_id',
            JUtils::MODEL_TABEL_FK_ONUPD => 'CASCADE',
            JUtils::MODEL_TABEL_FK_ONDEL => 'SET null',
            JUtils::MODEL_TABEL_FK_USEIT => true,
            JUtils::MODEL_TABEL_REQ_VAL  => 
                array(
                    array( 'JModelFamily' => 'f_id' ),
                    array( 'JModelFamily' => 'fs_name' )
                ),
            // if not defined -- use concat
            JUtils::MODEL_TABEL_REQ_FMTSTR => '%s (%s)',
            JUtils::MODEL_TABEL_REQ_FMTVAL => 
                array(
                    // first -- order # for sptintf by MODEL_TABEL_REQ_FMTSTR string
                    // second -- use translation of value
                    array( 1, false, ), // firstly goes role name, ...
                    array( 0, true ) // then only it's id
                )
        )
    ),
    
    
        
    'preset_model_JModelUser_extra' => array(
        '' => array(
            JUtils::MODEL_TABLE_EXTR_KEY => 'KEY `nick` (`user_nick`)'
        )
    ),
        
    'start_page' => 'index.php',
    
    'site_domain' => 'http://localhost',
]

?>