-- --------------------------------------------------------
-- Хост:                         localhost
-- Версия сервера:               5.6.12-log - MySQL Community Server (GPL)
-- ОС Сервера:                   Win32
-- HeidiSQL Версия:              9.3.0.4984
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
-- Дамп данных таблицы JDiary.jdairy_users: ~0 rows (приблизительно)
/*!40000 ALTER TABLE `jdairy_users` DISABLE KEYS */;
/*!40000 ALTER TABLE `jdairy_users` ENABLE KEYS */;

-- Дамп данных таблицы JDiary.jdiray_modelroles: ~5 rows (приблизительно)
/*!40000 ALTER TABLE `jdiray_modelroles` DISABLE KEYS */;
INSERT INTO `jdiray_modelroles` (`role`, `grants`) VALUES
	(0, 'X'),
	(1, 'A,B,C,E,F'),
	(2, 'A,B,D,E,F'),
	(3, 'A,B,E'),
	(4, 'A,B,C,D,E,F,G');
/*!40000 ALTER TABLE `jdiray_modelroles` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
