<?php

/**
 * @author ArtyomVeselkov
 * @copyright 2016
 */
 
 $jc = require( 'lib/core.php' );
 
 $jc->run( 'JUser' );
?>

<!DOCTYPE html>
<html lang="<?= $jc->view['lang']; ?>" >
<head>
    <title><?= $jc->view_translate( 'Specify you family' ); ?></title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    
    <link rel="stylesheet" href="static/css/bootstrap.min.css" type="text/css" /> 
    
    <body>
        <div class="navbar navbar-inverse" role="navigation" >           
            <div class="container">
                <div class="navbar-header">
                    <!--
                    <button type="button" class="navbar-toogle" data-toggle="collapse" data-target=".navbar-main-collapse"  />
                        <span class="sr-only">Toogle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    -->
                        <a href="<?= $jc->conf_get( 'site_domain' ); ?>" class="navbar-brand">
                            <?= $jc->view_translate( 'Trillium' ); ?>
                        </a>
                </div>
                <div class="collapse navbar-collapse navbar-main-collapse">
                    <ul class="nav navbar-nav">
                        <li>
                            <a href="profile.php" >
                                <?= $jc->view->translate( 'View Profile Page' ); ?>
                            </a>                            
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <section class="content">
                        <?php if ($jc->user_isLogged()): ?>
                            <h1>
                                <?= $jc->view_translate("In order to proceed to the diary you ought to create your family's account or choose already exists one"); ?>
                            </h1>                            
                        <?php endif; ?>                        
                    </section>
                    <?php if (!$jc->user_isLogged()): ?>
                        <div>
                            <h2>Create your family</h2>
                            <?= $jc->view[ 'box_register-family-form' ] ?>
                        </div>
                        <div>
                            <h2>Register</h2>
                            <?= $jc->view[ 'box_login-family-form' ] ?>
                        </div>
                    <?php endif; ?>
                    
                </div>
            </div>
        </div>
    </body>
    
</head>

</html>

<?php JLog::out() ?>
